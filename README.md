# (Un)Wetter


This mobile ready weather application provides a view on free weather data from [OpenWeatherMap](https://openweathermap.org/api). Data from the API is provided via a nginx based proxy. The API key is appended via rewrite rules and is therefore hidden from the user.

To get weather data the app requires you sharing the location. The location is stored in the local storage of your browser and is only used for calls to get the weather data based on coordinates.

Additional for German users weather alerts from NDR are shown.

A sample installation of this service can be found at https://unwetter.canceledsystems.com.

![(Un)Wetter APP](screenshots/screenshot.png "(Un)Wetter APP screenshot")

## Development

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.2.

### Development proxy to OpenWeatherMap

To get real weather data for development a proxy to OpenWeatherMap is needed.

#### 1. Get API key

You can get an API key for free from [OpenWeatherMap](https://openweathermap.org/api).

#### 2. Start nginx docker container

A simple way of setting up a proxy is via nginx and docker. Here is what an example configuration looks like.

```
server {
    listen       80;
    server_name  localhost;

    location /weather-api/ {
      set $args $args&appId=API_KEY;
      proxy_pass https://api.openweathermap.org/data/2.5/;
    }

    location /weather-map/ {
      set $args $args&appId=API_KEY;
      rewrite /weather-map/(.*) /map/$1.png  break;
      proxy_pass https://tile.openweathermap.org;
    }

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
```

Just replace `API_KEY` in the rewrite rules.

Then put the configuration in a file called `default.conf`.

Afterwards you can start the proxy by running the following command:

```
docker run -p "9000:80" -v "/PATH_TO_DEFAULT_CONF/default.conf:/etc/nginx/conf.d/default.conf" nginx
```

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

The development server is configured to use the nginx proxy at port 9000.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).


## Deployment

### Build the project

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Build docker image

```
docker build -t unwetter .
```

This builds the image and tags it as `unwetter`.

### Run docker container

```
docker run -p "80:80" -v "/PATH_TO_DEFAULT_CONF/default.conf:/etc/nginx/conf.d/default.conf" unwetter
```

This is just an example using HTTP. I recommend not using HTTP serving directly to the user. It's way better to use HTTPS, e.g. by configuring HTTPS for nginx or using an HTTPS server that proxies to the app container. 
I also recommend using a more stable setup for running the docker container like docker-compose docker-swarm or k8s.
