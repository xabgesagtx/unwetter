import { Pipe, PipeTransform } from '@angular/core';
import {DateTime} from 'luxon';

@Pipe({
  name: 'relativeCalendar'
})
export class RelativeCalendarPipe implements PipeTransform {

  transform(value: unknown): string {
    if (value instanceof Date) {
      return this.formatDateTime(DateTime.fromJSDate(value));
    } else if (value instanceof DateTime) {
      return this.formatDateTime(value);
    } else if (typeof value === 'string') {
      return this.formatDateTime(DateTime.fromISO(value));
    } else {
      return `unknown date type ${typeof value}`;
    }
  }

  private formatDateTime(dateTime: DateTime): string {
    return dateTime.toRelativeCalendar({locale: 'de-DE'}) + ' um ' + dateTime.toFormat('H:mm') + ' Uhr';
  }

}
