import {TestBed} from '@angular/core/testing';

import {MessageService} from './message.service';

describe('MessageService', () => {
  // beforeEach(() => TestBed.configureTestingModule({
  //   providers: [
  //     SnackbarService
  //   ]
  // }));

  it('should be created', () => {
    const service: MessageService = TestBed.get(MessageService);
    expect(service).toBeTruthy();
  });

  // it('should call snackbarservice on error', () => {
  //   const service: MessageService = TestBed.get(MessageService);
  //   const snackbarService = TestBed.get(SnackbarService);
  //   spyOn(snackbarService, 'add');
  //   service.showError('Error');
  //   expect(snackbarService.add).toHaveBeenCalledWith({
  //     msg: 'Error',
  //     color: ERROR_COLOR,
  //     action: {
  //       text: ''
  //     }
  //   });
  // });
  //
  // it('should call snackbarservice on warning', () => {
  //   const service: MessageService = TestBed.get(MessageService);
  //   const snackbarService = TestBed.get(SnackbarService);
  //   spyOn(snackbarService, 'add');
  //   service.showWarning('Warning');
  //   expect(snackbarService.add).toHaveBeenCalledWith({
  //     msg: 'Warning',
  //     color: WARNING_COLOR,
  //     action: {
  //       text: ''
  //     }
  //   });
  // });
  //
  // it('should call snackbarservice on success', () => {
  //   const service: MessageService = TestBed.get(MessageService);
  //   const snackbarService = TestBed.get(SnackbarService);
  //   spyOn(snackbarService, 'add');
  //   service.showSuccess('Success');
  //   expect(snackbarService.add).toHaveBeenCalledWith({
  //     msg: 'Success',
  //     action: {
  //       text: ''
  //     }
  //   });
  // });
});
