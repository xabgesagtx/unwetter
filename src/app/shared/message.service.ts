import {Injectable} from '@angular/core';
import {Toast} from './model/toast';

export const ERROR_COLOR = '#CC1111';
export const WARNING_COLOR = '#DDDD11';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  toasts: Toast[] = [];

  showError(message: string) {
    const classname = "bg-danger text-light";
    this.toasts.push({message, classname});
  }

  showWarning(message: string) {
    const classname = "bg-warning text-light";
    this.toasts.push({message, classname});
  }

  showSuccess(message: string) {
    const classname = "bg-success text-light";
    this.toasts.push({message, classname});
  }

  remove(toast: Toast) {
    this.toasts = this.toasts.filter((t) => t !== toast);
  }

}
