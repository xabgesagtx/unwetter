import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoadingSpinnerComponent} from './loading-spinner/loading-spinner.component';
import {MessageService} from './message.service';
import {MessagesComponent} from './messages/messages.component';
import {NgbToastModule} from '@ng-bootstrap/ng-bootstrap';
import { RelativeCalendarPipe } from './relative-calendar.pipe';

@NgModule({
  declarations: [
    LoadingSpinnerComponent,
    MessagesComponent,
    RelativeCalendarPipe
  ],
    exports: [
        LoadingSpinnerComponent,
        MessagesComponent,
        RelativeCalendarPipe
    ],
  imports: [
    CommonModule,
    NgbToastModule
  ],
  providers: [
    MessageService
  ]
})
export class SharedModule { }
