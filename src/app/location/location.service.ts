import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {NominatimResult} from './model/external/nominatim-result';
import {MessageService} from '../shared/message.service';
import {Coordinates} from './model/internal/coordinates';
import {Location} from './model/internal/location';
import {NominatimAddress} from './model/external/nominatim-address';
import {Address} from './model/internal/address';

const NOMINATIM_BASE_URL = `https://nominatim.openstreetmap.org/reverse`;

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  private locationChanged$ = new EventEmitter<Location>();
  private location: Location;

  constructor(private httpClient: HttpClient, private messageService: MessageService) { }

  private static convertToAddress(address: NominatimAddress): Address {
    return {
      street: address.road,
      houseNumber: address.house_number,
      city: address.city_district,
      zipcode: address.postcode,
      country: address.country
    };
  }

  isLocationConfigured() {
    return Boolean(this.location);
  }

  getCurrentLocation() {
    return this.location;
  }

  private setCurrentLocation(location: Location) {
    this.location = location;
    this.locationChanged$.emit(location);
  }

  setLocationFromBrowser() {
    navigator.geolocation.getCurrentPosition(position => {
      this.retrieveAddress({
        longitude: position.coords.longitude,
        latitude: position.coords.latitude
      });
    }, positionError => this.messageService.showError(`Konnte Standort nicht bestimmen: ${positionError.message}`)
    );
  }

  retrieveAddress(coordinates: Coordinates) {
    this.httpClient.get<NominatimResult>(`${NOMINATIM_BASE_URL}?format=jsonv2&lat=${coordinates.latitude}&lon=${coordinates.longitude}`)
      .subscribe(result => {
        const address = LocationService.convertToAddress(result.address);
        this.setCurrentLocation({
          coordinates: coordinates,
          address: address
        });
        this.persistToStorage();
        this.messageService.showSuccess('Standort aktualisiert');
      }, (error: HttpErrorResponse) => {
        this.setCurrentLocation({
          coordinates: coordinates
        });
        this.persistToStorage();
        this.messageService.showWarning(`Standort aktualisiert. Adresse konnte nicht bestimmt werden: ${error.message}`);
      });
  }

  loadFromStorage() {
    const locationValue = localStorage.getItem('location');
    if (locationValue) {
      const parsedLocation = JSON.parse(locationValue);
      if (parsedLocation.coordinates) {
        this.setCurrentLocation(parsedLocation);
      } else {
        console.log(`Failed to parse message from string: ${locationValue}`);
      }
    }
  }

  persistToStorage() {
    localStorage.setItem('location', JSON.stringify(this.location));
  }

  /* eslint-disable @typescript-eslint/no-explicit-any */
  subscribe(callback: any) {
    this.locationChanged$.subscribe(callback);
  }
}
