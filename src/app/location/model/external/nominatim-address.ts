export class NominatimAddress {
  house_number: string;
  road: string;
  neighbourhood: string;
  suburb: string;
  city_district: string;
  state: string;
  postcode: string;
  country: string;
}
