import {NominatimAddress} from './nominatim-address';

export class LocationWrapper {
  longitude: number;
  latitude: number;
  address: NominatimAddress | undefined;
}
