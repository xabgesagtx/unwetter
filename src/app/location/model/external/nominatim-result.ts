import {NominatimAddress} from './nominatim-address';

export class NominatimResult {
  address: NominatimAddress;
}
