export class Address {
  street: string;
  houseNumber: string;
  city: string;
  zipcode: string;
  country: string;
}
