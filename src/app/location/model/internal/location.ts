import {Address} from './address';
import {Coordinates} from './coordinates';

export class Location {
  coordinates: Coordinates;
  address?: Address;
}
