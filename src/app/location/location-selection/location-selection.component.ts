import {Component} from '@angular/core';
import {LocationService} from '../location.service';
import {faMapMarkedAlt} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-location-selection',
  templateUrl: './location-selection.component.html',
  styleUrls: ['./location-selection.component.scss']
})
export class LocationSelectionComponent {

  faMapMarkedAlt = faMapMarkedAlt;

  constructor(private locationService: LocationService) { }

  setCurrentLocation() {
    this.locationService.setLocationFromBrowser();
  }

  getLocation() {
    return this.locationService.getCurrentLocation();
  }

}
