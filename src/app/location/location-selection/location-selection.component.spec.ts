import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LocationSelectionComponent} from './location-selection.component';
import {LocationService} from '../location.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('LocationSelectionComponent', () => {
  let component: LocationSelectionComponent;
  let fixture: ComponentFixture<LocationSelectionComponent>;

  beforeEach(async(() => {
    const locationServiceSpy = jasmine.createSpyObj('LocationService', ['getCurrentLocation']);

    TestBed.configureTestingModule({
      declarations: [
        LocationSelectionComponent
      ],
      providers: [
        { provide: LocationService, useValue: locationServiceSpy}
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
