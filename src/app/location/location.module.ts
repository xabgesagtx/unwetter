import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {HttpClientModule} from '@angular/common/http';
import {SharedModule} from '../shared/shared.module';
import {LocationService} from './location.service';
import {LocationSelectionComponent} from './location-selection/location-selection.component';

@NgModule({
  declarations: [
    LocationSelectionComponent
  ],
  exports: [
    LocationSelectionComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [
    LocationService
  ]
})
export class LocationModule { }
