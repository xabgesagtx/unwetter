import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ForecastCarouselComponent} from './forecast-carousel.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('ForecastCarouselComponent', () => {
  let component: ForecastCarouselComponent;
  let fixture: ComponentFixture<ForecastCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ForecastCarouselComponent
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
