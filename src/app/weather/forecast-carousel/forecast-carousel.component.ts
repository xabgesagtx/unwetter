import {Component, Input, ViewChild} from '@angular/core';
import {faArrowLeft, faArrowRight} from '@fortawesome/free-solid-svg-icons';
import {ForecastData} from '../model/internal/forecast-data';

@Component({
  selector: 'app-forecast-carousel',
  templateUrl: './forecast-carousel.component.html',
  styleUrls: ['./forecast-carousel.component.scss']
})
export class ForecastCarouselComponent {

  @Input() forecasts: ForecastData[];
  @Input() loading: boolean;
  @ViewChild('forecastList') forecastList;

  faArrowLeft = faArrowLeft;
  faArrowRight = faArrowRight;

  scrollRight() {
    this.forecastList.nativeElement.scrollBy(400, 0);
  }

  scrollLeft() {
    this.forecastList.nativeElement.scrollBy(-400, 0);
  }

}
