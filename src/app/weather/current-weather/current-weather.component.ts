import {Component, Input} from '@angular/core';
import {WeatherInfo} from '../model/internal/weather-info';

export const ICON_BASE_URL = 'https://openweathermap.org/img/w/';

@Component({
  selector: 'app-current-weather',
  templateUrl: './current-weather.component.html',
  styleUrls: ['./current-weather.component.scss']
})
export class CurrentWeatherComponent {

  @Input() weather: WeatherInfo;
  @Input() loading: boolean;

}
