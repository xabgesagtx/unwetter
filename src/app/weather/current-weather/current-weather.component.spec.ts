import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CurrentWeatherComponent} from './current-weather.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';


describe('CurrentWeatherComponent', () => {
  let component: CurrentWeatherComponent;
  let fixture: ComponentFixture<CurrentWeatherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CurrentWeatherComponent
      ],
      imports: [
        SharedModule
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentWeatherComponent);
    component = fixture.componentInstance;
    component.weather = {
      location: 'name',
      datetime: new Date(),
      descriptions: [
        {
          text: 'description',
          iconUrl: 'https://example.com/icon.png'
        }
      ],
      temp: 2,
      minTemp: 1,
      maxTemp: 3,
      humidity: 40
    };
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should render weather icon', () => {
    const weatherIcon = fixture.nativeElement.querySelector('.weather-icons img');
    expect(weatherIcon.attributes.getNamedItem('src').value).toEqual(component.weather.descriptions[0].iconUrl);
    expect(weatherIcon.attributes.getNamedItem('alt').value).toEqual(component.weather.descriptions[0].text);
    expect(weatherIcon.attributes.getNamedItem('title').value).toEqual(component.weather.descriptions[0].text);
  });
  it('should render weather data', () => {
    const headline = fixture.nativeElement.querySelector('h2').textContent;
    expect(headline).toEqual('2°C');
    const minMax = fixture.nativeElement.querySelector('.min-max').textContent;
    expect(minMax).toEqual('1°C / 3°C');
    const humidity = fixture.nativeElement.querySelector('.humidity').textContent;
    expect(humidity).toEqual('Luftfeuchtigkeit: 40%');
    const date = fixture.nativeElement.querySelector('.weather-date').textContent;
    expect(date).toContain('heute um');
  });

});
