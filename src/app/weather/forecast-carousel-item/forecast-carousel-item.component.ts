import {Component, Input} from '@angular/core';
import {ForecastData} from '../model/internal/forecast-data';

@Component({
  selector: 'app-forecast-carousel-item',
  templateUrl: './forecast-carousel-item.component.html',
  styleUrls: ['./forecast-carousel-item.component.scss']
})
export class ForecastCarouselItemComponent {

  @Input() forecast: ForecastData;

}
