import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ForecastCarouselItemComponent} from './forecast-carousel-item.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';

describe('ForecastCarouselItemComponent', () => {
  let component: ForecastCarouselItemComponent;
  let fixture: ComponentFixture<ForecastCarouselItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ForecastCarouselItemComponent
      ],
      imports: [
        SharedModule
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastCarouselItemComponent);
    component = fixture.componentInstance;
    component.forecast = {
      datetime: new Date(0),
      descriptions: [
        {
          text: 'description',
          iconUrl: 'https://example.com/icon.png'
        }
      ],
      minTemp: 1,
      maxTemp: 3,
      temp: 2,
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should render weather icon', () => {
    const weatherIcon = fixture.nativeElement.querySelector('.weather-icons img');
    expect(weatherIcon.attributes.getNamedItem('src').value).toEqual(component.forecast.descriptions[0].iconUrl);
    expect(weatherIcon.attributes.getNamedItem('alt').value).toEqual(component.forecast.descriptions[0].text);
    expect(weatherIcon.attributes.getNamedItem('title').value).toEqual(component.forecast.descriptions[0].text);
  });
});
