import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {WeatherWrapper} from './model/external/weather-wrapper';
import {Forecast} from './model/external/forecast';
import {WeatherInfo} from './model/internal/weather-info';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {WeatherDescription} from './model/external/weather-description';
import {Description} from './model/internal/description';
import {ForecastData} from './model/internal/forecast-data';

export const ICON_BASE_URL = 'https://openweathermap.org/img/w/';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private httpClient: HttpClient) { }

  private baseUrl = '/weather-api/';
  private todayUrl = `${this.baseUrl}weather`;
  private forecastUrl = `${this.baseUrl}forecast`;
  private latParam = 'lat';
  private lonParam = 'lon';

  private static convertWrapperToInfo(wrapper: WeatherWrapper): WeatherInfo {
    const descriptions = wrapper.weather.map(description => WeatherService.convertDescription(description));
    return {
      location: wrapper.name,
      datetime: new Date(wrapper.dt * 1000),
      temp: wrapper.main.temp,
      maxTemp: wrapper.main.temp_max,
      minTemp: wrapper.main.temp_min,
      humidity: wrapper.main.humidity,
      descriptions: descriptions,
    };
  }

  private static convertForecast(forecast: Forecast): ForecastData[] {
    return forecast.list.map(wrapper => WeatherService.convertWrapperToForecastData(wrapper));
  }

  private static convertWrapperToForecastData(wrapper: WeatherWrapper): ForecastData {
    const descriptions = wrapper.weather.map(description => WeatherService.convertDescription(description));
    return {
      datetime: new Date(wrapper.dt * 1000),
      temp: wrapper.main.temp,
      minTemp: wrapper.main.temp_min,
      maxTemp: wrapper.main.temp_max,
      descriptions: descriptions,
    };
  }

  private static convertDescription(description: WeatherDescription): Description {
    return {
      text: description.description,
      iconUrl: `${ICON_BASE_URL}${description.icon}.png`
    };
  }

  getToday(lat: number, lon: number): Observable<WeatherInfo> {
    const params = new HttpParams()
      .append(this.latParam, lat.toString())
      .append(this.lonParam, lon.toString())
      .append('units', 'Metric');
    return this.httpClient.get<WeatherWrapper>(this.todayUrl, { params: params })
      .pipe(
        map(wrapper => WeatherService.convertWrapperToInfo(wrapper))
      );
  }

  getForecasts(lat: number, lon: number): Observable<ForecastData[]> {
    const params = new HttpParams()
      .append(this.latParam, lat.toString())
      .append(this.lonParam, lon.toString())
      .append('units', 'Metric');
    return this.httpClient.get<Forecast>(this.forecastUrl, { params: params })
      .pipe(
        map(forecast => WeatherService.convertForecast(forecast))
      );
  }
}
