import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {WeatherOverviewComponent} from './weather-overview.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {WeatherService} from '../weather.service';
import {LocationService} from '../../location/location.service';
import {MessageService} from '../../shared/message.service';

describe('WeatherOverviewComponent', () => {
  let component: WeatherOverviewComponent;
  let fixture: ComponentFixture<WeatherOverviewComponent>;
  const locationServiceSpy = jasmine.createSpyObj('LocationService', ['subscribe', 'isLocationConfigured']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);
  const weatherServiceSpy = jasmine.createSpyObj('WeatherService', ['loadCurrentWeather', 'loadForecast']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        WeatherOverviewComponent
      ],
      providers: [
        { provide: LocationService, useValue: locationServiceSpy },
        { provide: MessageService, useValue: messageServiceSpy },
        { provide: WeatherService, useValue: weatherServiceSpy },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
