import {Component, OnInit} from '@angular/core';
import {WeatherService} from '../weather.service';
import {LocationService} from '../../location/location.service';
import {HttpErrorResponse} from '@angular/common/http';
import {faSyncAlt} from '@fortawesome/free-solid-svg-icons';
import {MessageService} from '../../shared/message.service';
import {WeatherInfo} from '../model/internal/weather-info';
import {ForecastData} from '../model/internal/forecast-data';
import {Coordinates} from '../../location/model/internal/coordinates';
import {Location} from '../../location/model/internal/location';

@Component({
  selector: 'app-weather-overview',
  templateUrl: './weather-overview.component.html',
  styleUrls: ['./weather-overview.component.scss']
})
export class WeatherOverviewComponent implements OnInit {

  faSyncAlt = faSyncAlt;
  weather: WeatherInfo;
  forecasts: ForecastData[];
  weatherLoading = false;
  forecastsLoading = false;

  constructor(private weatherService: WeatherService, private locationService: LocationService, private messageService: MessageService) {
  }

  ngOnInit() {
    this.locationService.subscribe((currentLocation: Location) => {
      this.loadWeatherForLocation(currentLocation.coordinates);
      this.loadForecastsForLocation(currentLocation.coordinates);
    });
  }

  private loadForecastsForLocation(coordinates: Coordinates) {
    this.forecastsLoading = true;
    this.weatherService.getForecasts(coordinates.latitude, coordinates.longitude)
      .subscribe(forecasts => {
        this.forecasts = forecasts;
      }, (error: HttpErrorResponse) =>
        this.messageService.showError(`Konnte Wettervorhersage nicht laden: ${error.message}`),
        () => this.forecastsLoading = false);
  }

  private loadWeatherForLocation(coordinates: Coordinates) {
    this.weatherLoading = true;
    this.weatherService.getToday(coordinates.latitude, coordinates.longitude)
      .subscribe(weather => {
        this.weather = weather;
      }, (error: HttpErrorResponse) => this.messageService.showError(`Konnte Wetterdaten nicht laden: ${error.message}`),
        () => this.weatherLoading = false);
  }

  updateWeather() {
    this.loadWeatherForLocation(this.locationService.getCurrentLocation().coordinates);
    this.loadForecastsForLocation(this.locationService.getCurrentLocation().coordinates);
  }

  isLocationConfigured() {
    return this.locationService.isLocationConfigured();
  }

  getLatitude() {
    if (this.locationService.isLocationConfigured()) {
      return this.locationService.getCurrentLocation().coordinates.latitude;
    }
  }

  getLongitude() {
    if (this.locationService.isLocationConfigured()) {
      return this.locationService.getCurrentLocation().coordinates.longitude;
    }
  }
}
