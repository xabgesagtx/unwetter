import {AfterViewChecked, Component, Input, OnChanges, OnInit} from '@angular/core';
import * as L from 'leaflet';
import 'leaflet-fullscreen';

@Component({
  selector: 'app-weather-map',
  templateUrl: './weather-map.component.html',
  styleUrls: ['./weather-map.component.scss']
})
export class WeatherMapComponent implements OnInit, OnChanges, AfterViewChecked {

  @Input() lat: number;
  @Input() lon: number;
  map: L.Map;

  ngOnInit() {
    this.setUpMap();
  }

  ngOnChanges(): void {
    this.recenterMap();
  }

  private recenterMap() {
    if (this.map) {
      const latLng = L.latLng(this.lat, this.lon);
      this.map.panTo(latLng);
      this.map.invalidateSize(true);
    }
  }

  private setUpMap() {
    const carto = this.cartoLayer();
    const rain = this.rainLayer();
    const latLng = L.latLng(this.lat, this.lon);

    this.map = L.map('weather-map', {
      center: latLng,
      zoom: 11,
      minZoom: 8,
      maxZoom: 18,
      layers: [carto, rain],
      fullscreenControl: true
    });
  }

  private cartoLayer() {
    return L.tileLayer(
      'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png',
      {
        minZoom: 8,
        maxZoom: 18
      });
  }

  private rainLayer() {
    const weatherAttr = 'Weather from <a href="https://openweathermap.org/" ' +
      'title="World Map and worldwide Weather Forecast online">OpenWeatherMap</a>';
    return L.tileLayer(
      '/weather-map/precipitation_new/{z}/{x}/{y}',
      {
        minZoom: 8,
        maxZoom: 18,
        attribution: weatherAttr
      }
    );
  }

  ngAfterViewChecked(): void {
    this.map.invalidateSize(true);
  }

}
