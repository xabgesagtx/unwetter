import {Description} from './description';

export class ForecastData {
  datetime: Date;
  temp: number;
  minTemp: number;
  maxTemp: number;
  descriptions: Description[];
}
