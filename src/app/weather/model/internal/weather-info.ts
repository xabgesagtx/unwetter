import {Description} from './description';

export class WeatherInfo {
  location: string;
  datetime: Date;
  temp: number;
  maxTemp: number;
  minTemp: number;
  humidity: number;
  descriptions: Description[];
}
