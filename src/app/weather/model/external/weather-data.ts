export class WeatherData {
  temp: number;
  humidity: number;
  temp_min: number;
  temp_max: number;
}
