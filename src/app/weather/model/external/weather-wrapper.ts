import {WeatherDescription} from './weather-description';
import {WeatherData} from './weather-data';

export class WeatherWrapper {
  name: string;
  dt: number;
  weather: WeatherDescription[];
  main: WeatherData;
}
