import {WeatherWrapper} from './weather-wrapper';
import {City} from './city';

export class Forecast {
  city: City;
  list: WeatherWrapper[];
}
