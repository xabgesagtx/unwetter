export class WeatherDescription {
  main: string;
  description: string;
  icon: string;
}
