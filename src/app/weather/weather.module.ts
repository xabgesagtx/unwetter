import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CurrentWeatherComponent} from './current-weather/current-weather.component';
import {ForecastCarouselComponent} from './forecast-carousel/forecast-carousel.component';
import {ForecastGraphComponent} from './forecast-graph/forecast-graph.component';
import {WeatherMapComponent} from './weather-map/weather-map.component';
import {WeatherOverviewComponent} from './weather-overview/weather-overview.component';
import {HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {LocationModule} from '../location/location.module';
import {SharedModule} from '../shared/shared.module';
import {WeatherService} from './weather.service';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgbNavModule} from '@ng-bootstrap/ng-bootstrap';
import { ForecastCarouselItemComponent } from './forecast-carousel-item/forecast-carousel-item.component';

@NgModule({
  declarations: [
    CurrentWeatherComponent,
    ForecastCarouselComponent,
    ForecastGraphComponent,
    WeatherMapComponent,
    WeatherOverviewComponent,
    ForecastCarouselItemComponent
  ],
  exports: [
    WeatherOverviewComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    FontAwesomeModule,
    HttpClientModule,
    LocationModule,
    NgbNavModule,
    SharedModule
  ],
  providers: [
    WeatherService
  ]
})
export class WeatherModule { }
