import {AfterViewInit, Component, ElementRef, Input, ViewChild} from '@angular/core';
import {DateTime} from 'luxon';
import {Chart, LinearScale, LineController, LineElement, PointElement, TimeScale, Tooltip} from 'chart.js';
import 'chartjs-adapter-luxon';
import {ForecastData} from '../model/internal/forecast-data';

@Component({
  selector: 'app-forecast-graph',
  templateUrl: './forecast-graph.component.html',
  styleUrls: ['./forecast-graph.component.scss']
})
export class ForecastGraphComponent implements AfterViewInit {

  @ViewChild('forecastGraphCanvas') canvas: ElementRef;

  private _forecasts: ForecastData[];
  private forecastGraphData: { x: DateTime, y: number }[] = [];

  @Input() set forecasts(data: ForecastData[]) {
    this._forecasts = data;
    this.forecastGraphData.length = 0;
    data.map(forecast => {
      this.forecastGraphData.push({
        x: DateTime.fromJSDate(forecast.datetime),
        y: forecast.temp
      })
    });
    if (this.forecastChart) {
      this.forecastChart.update();
    }
  }
  get forecasts(): ForecastData[] {
    return this._forecasts;
  }
  @Input() loading: boolean;

  private forecastChart: Chart;

  ngAfterViewInit() {
    Chart.register(LineController, TimeScale, LinearScale, PointElement, LineElement, Tooltip);
    this.forecastChart = new Chart(this.canvas.nativeElement, {
      type: 'line',
      data: {
        datasets: [
          {
            label: 'Temperatur',
            /* eslint-disable @typescript-eslint/no-explicit-any */
            data: this.forecastGraphData as any
          }
        ]
      },
      options: {
        scales: {
          x: {
            type: 'time',
            ticks:{
              stepSize: 12
            },
            time: {
              tooltipFormat: 'DD T',
              unit: 'hour',
              displayFormats: {
                hour: 'H:mm cccc'
              }
            },
            title: {
              display: true,
              text: 'Date'
            }
          }
        }
      }
    });
  }

}
