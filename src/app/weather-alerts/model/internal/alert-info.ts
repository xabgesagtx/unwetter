export class AlertInfo {
  headline: string;
  description: string;
  instruction: string;
  startDate: Date;
  endDate: Date;
}
