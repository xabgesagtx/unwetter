export class WeatherAlert {
  headline: string;
  description: string;
  instruction: string;
  polygon: number[][][];
  onset: string;
  expires: string;
}
