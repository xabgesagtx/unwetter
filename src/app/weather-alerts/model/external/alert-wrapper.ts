import {WeatherAlert} from './weather-alert';

export class AlertWrapper {
  alerts: WeatherAlert[];
}
