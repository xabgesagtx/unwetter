import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AlertComponent} from './alert.component';
import {SharedModule} from '../../shared/shared.module';

describe('AlertComponent', () => {
  let component: AlertComponent;
  let fixture: ComponentFixture<AlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AlertComponent],
      imports: [SharedModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertComponent);
    component = fixture.componentInstance;
  });

  it('should display properly formatted text', () => {
    const headline = 'Headline';
    const description = 'Description';
    const instruction = 'Instruction';
    component.weatherAlert = {
      headline: headline,
      description: description,
      instruction: instruction,
      startDate: new Date(),
      endDate: new Date()
    };
    fixture.detectChanges();
    const headlineElement = fixture.nativeElement.querySelector('.card-title');
    const descriptionElement = fixture.nativeElement.querySelector('.alert-description');
    const instructionElement = fixture.nativeElement.querySelector('.alert-instruction');
    const fromToElement = fixture.nativeElement.querySelector('.from-to');
    expect(headlineElement.textContent).toEqual(headline);
    expect(descriptionElement.textContent).toEqual(description);
    expect(instructionElement.textContent).toEqual(instruction);
    expect(fromToElement.textContent).toContain('heute um');
    expect(fromToElement.textContent).toContain(' bis ');
  });
});
