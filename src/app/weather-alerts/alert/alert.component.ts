import {Component, Input} from '@angular/core';
import {AlertInfo} from '../model/internal/alert-info';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent {

  @Input()
  weatherAlert: AlertInfo;

  get showInstruction(): boolean {
    return typeof this.weatherAlert.instruction === 'string';
  }

}
