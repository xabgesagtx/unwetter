import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AlertsComponent} from './alerts.component';
import {LocationService} from '../../location/location.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {AlertService} from '../alert.service';
import {MessageService} from '../../shared/message.service';

describe('AlertsComponent', () => {
  let component: AlertsComponent;
  let fixture: ComponentFixture<AlertsComponent>;
  const alertServiceSpy = jasmine.createSpyObj('AlertService', ['getAlerts']);
  const locationServiceSpy = jasmine.createSpyObj('LocationService', ['subscribe', 'isLocationConfigured']);
  const messageServiceSpy = jasmine.createSpyObj('MessageService', ['showError']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AlertsComponent
      ],
      providers: [
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: LocationService, useValue: locationServiceSpy },
        { provide: MessageService, useValue: messageServiceSpy },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
