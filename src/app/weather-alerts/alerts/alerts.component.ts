import {Component, OnInit} from '@angular/core';
import {AlertService} from '../alert.service';
import {HttpErrorResponse} from '@angular/common/http';
import {LocationService} from '../../location/location.service';
import {faSyncAlt} from '@fortawesome/free-solid-svg-icons';
import {MessageService} from '../../shared/message.service';
import {Location} from '../../location/model/internal/location';
import {AlertInfo} from '../model/internal/alert-info';
import {Coordinates} from '../../location/model/internal/coordinates';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss']
})
export class AlertsComponent implements OnInit {

  alerts: AlertInfo[] = [];
  faSyncAlt = faSyncAlt;
  loading = false;

  constructor(private alertService: AlertService, private locationService: LocationService, private messageService: MessageService) { }

  ngOnInit() {
    this.locationService.subscribe((location: Location) => this.loadAlerts(location.coordinates));
  }

  private loadAlerts(coordinates: Coordinates) {
    this.loading = true;
    this.alertService.getAlerts(coordinates)
      .subscribe(result => this.alerts = result,
        (error: HttpErrorResponse) => this.messageService.showError(`Wetterwarnungen konnten nicht aktualisiert werden: ${error.message}`),
        () => this.loading = false);
  }

  updateAlerts() {
    if (this.locationService.isLocationConfigured()) {
      const location = this.locationService.getCurrentLocation();
      this.loadAlerts(location.coordinates);
    }
  }

  isLocationConfigured() {
    return this.locationService.isLocationConfigured();
  }

}
