import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AlertWrapper} from './model/external/alert-wrapper';
import {map} from 'rxjs/operators';
import * as inside from 'point-in-polygon';
import {AlertInfo} from './model/internal/alert-info';
import {WeatherAlert} from './model/external/weather-alert';
import {Observable} from 'rxjs';
import {Coordinates} from '../location/model/internal/coordinates';

const ALERTS_URL = 'https://www.ndr.de/public/unwetter/json/alerts.json';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private httpClient: HttpClient) {
  }

  private static isInPolygons(polygons: number[][][], coordinates: Coordinates) {
    if (!polygons) {
      return false;
    }
    const coordinatesArray = [coordinates.longitude, coordinates.latitude];
    for (let i = 0; i < polygons.length; i++) {
      const polygon = polygons[i];
      if (inside(coordinatesArray, polygon)) {
        return true;
      }
    }
    return false;
  }

  private static convertToAlertInfo(alert: WeatherAlert): AlertInfo {
    return {
      headline: alert.headline,
      description: alert.description,
      instruction: alert.instruction,
      startDate: new Date(alert.onset),
      endDate: new Date(alert.expires)
    };
  }

  getAlerts(coordinates: Coordinates): Observable<AlertInfo[]> {
    return this.httpClient.get<AlertWrapper>(ALERTS_URL)
      .pipe(
        map(wrapper => wrapper.alerts.filter(alert => AlertService.isInPolygons(alert.polygon, coordinates))
          .map(alert => AlertService.convertToAlertInfo(alert))
        )
      );
  }
}
