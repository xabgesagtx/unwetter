import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {AlertComponent} from './alert/alert.component';
import {AlertsComponent} from './alerts/alerts.component';
import {AlertService} from './alert.service';
import {SharedModule} from '../shared/shared.module';
import {LocationModule} from '../location/location.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    AlertComponent,
    AlertsComponent
  ],
  exports: [
    AlertsComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    HttpClientModule,
    LocationModule,
    SharedModule
  ],
  providers: [
    AlertService
  ]
})
export class WeatherAlertsModule { }
