import {Component, OnInit} from '@angular/core';
import {LocationService} from './location/location.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private locationService: LocationService) {}

  ngOnInit() {
    setTimeout(() => this.locationService.loadFromStorage(), 100);
  }
}
