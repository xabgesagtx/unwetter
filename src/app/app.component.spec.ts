import {async, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {LocationService} from './location/location.service';

describe('AppComponent', () => {

  const locationServiceSpy = jasmine.createSpyObj('LocationService', ['loadFromStorage']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: LocationService, useValue: locationServiceSpy }
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should load location from storage`, fakeAsync(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    tick(101);
    const locationService = TestBed.get(LocationService);
    expect(locationService.loadFromStorage).toHaveBeenCalled();
  }));

});
