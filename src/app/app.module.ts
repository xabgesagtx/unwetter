import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {LocationModule} from './location/location.module';
import {WeatherModule} from './weather/weather.module';
import {WeatherAlertsModule} from './weather-alerts/weather-alerts.module';
import {SharedModule} from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    LocationModule,
    WeatherModule,
    WeatherAlertsModule,
    SharedModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
